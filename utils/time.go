package utils

import (
	"sort"
	"strings"
	"time"
)

type Times []string

func (t Times) Len() int {
	return len(t)
}

func (t Times) Less(i, j int) bool {
	ti, err := time.Parse("15:04", t[i])
	if err != nil {
		return strings.Compare(t[i], t[j]) < 0
	}
	tj, err := time.Parse("15:04", t[j])
	if err != nil {
		return strings.Compare(t[i], t[j]) < 0
	}
	return ti.Before(tj)
}

func (t Times) Swap(i, j int) {
	t[i], t[j] = t[j], t[i]
}

func SortTimes(times []string) []string {
	sort.Sort(Times(times))
	return times
}
