package utils

func GetIndexByValue[T comparable](slice []T, value T) int {
	for i, v := range slice {
		if v == value {
			return i
		}
	}
	return -1
}

func RemoveItemFromArray[T any](arr []T, index int) []T {
	if index < 0 || index >= len(arr) {
		return arr
	}

	result := make([]T, len(arr)-1)
	copy(result, arr[:index])
	copy(result[index:], arr[index+1:])

	return result
}
