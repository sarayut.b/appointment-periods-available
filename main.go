package main

import (
	"fmt"
	"periods/utils"
)

type Appointment struct {
	ID                   string
	DoctorID             string
	SubTreatmentDuration int
	AppointmentedAt      string
}

func appointmentMock() []Appointment {
	return []Appointment{
		{
			ID:                   "1",
			DoctorID:             "D1",
			SubTreatmentDuration: 5,
			AppointmentedAt:      "09:00",
		},
		{
			ID:                   "2",
			DoctorID:             "D1",
			SubTreatmentDuration: 5,
			AppointmentedAt:      "09:00",
		},
		{
			ID:                   "3",
			DoctorID:             "D1",
			SubTreatmentDuration: 45,
			AppointmentedAt:      "09:30",
		},
		{
			ID:                   "4",
			DoctorID:             "D2",
			SubTreatmentDuration: 9,
			AppointmentedAt:      "09:00",
		},
		{
			ID:                   "5",
			DoctorID:             "D2",
			SubTreatmentDuration: 12,
			AppointmentedAt:      "09:00",
		},
		{
			ID:                   "6",
			DoctorID:             "D2",
			SubTreatmentDuration: 65,
			AppointmentedAt:      "09:30",
		},
	}
}

// func doctorPeriodMock() map[string]map[string]int {
// 	doctorPeriodDuration := make(map[string]map[string]int)
// 	doctorPeriodDuration["09:00"] = map[string]int{
// 		"D1": 0,
// 		"D2": 0,
// 	}
// 	doctorPeriodDuration["09:30"] = map[string]int{
// 		"D1": 0,
// 		"D2": 0,
// 	}
// 	doctorPeriodDuration["10:00"] = map[string]int{
// 		"D1": 0,
// 		"D2": 0,
// 	}
// 	doctorPeriodDuration["10:30"] = map[string]int{
// 		"D1": 0,
// 		"D2": 0,
// 	}

// 	return doctorPeriodDuration
// }

func doctorPeriodMock() map[string]map[string]int {
	doctorPeriodDuration := make(map[string]map[string]int)
	doctorPeriodDuration["09:00"] = map[string]int{
		"D1": 0,
		"D2": 0,
	}
	doctorPeriodDuration["09:30"] = map[string]int{
		"D1": 0,
		"D2": 0,
	}
	doctorPeriodDuration["10:00"] = map[string]int{
		"D1": 0,
	}
	doctorPeriodDuration["10:30"] = map[string]int{
		"D1": 0,
	}
	doctorPeriodDuration["11:00"] = map[string]int{
		"D1": 0,
		"D2": 0,
	}
	doctorPeriodDuration["11:30"] = map[string]int{
		"D1": 0,
		"D2": 0,
	}

	return doctorPeriodDuration
}

func main() {
	doctorPeriodDuration := doctorPeriodMock()
	appointments := appointmentMock()
	resPeriods := []string{"09:00", "09:30", "10:00", "10:30"}
	periodList := []string{"09:00", "09:30", "10:00", "10:30"}

	subTreatmentDuration := 15
	maxDuration := 30

	// init doctor periods
	for _, appointment := range appointments {
		totalDuration := doctorPeriodDuration[appointment.AppointmentedAt][appointment.DoctorID] + appointment.SubTreatmentDuration

		index := 0
		for subPeriod := totalDuration; subPeriod > 0; subPeriod -= maxDuration {
			periodIndex := getIndexByValue(periodList, appointment.AppointmentedAt)
			nextPeriodIndex := periodIndex + index
			if periodIndex < 0 || nextPeriodIndex > len(periodList)-1 {
				break
			}

			// Prevent doctor out of period
			if _, ok := doctorPeriodDuration[periodList[nextPeriodIndex]][appointment.DoctorID]; ok {
				if totalDuration > maxDuration {
					doctorPeriodDuration[periodList[nextPeriodIndex]][appointment.DoctorID] = maxDuration
				} else {
					doctorPeriodDuration[periodList[nextPeriodIndex]][appointment.DoctorID] = subPeriod
				}
			}

			totalDuration -= maxDuration
			index += 1
		}
	}

	fmt.Printf("\n %+v \n", resPeriods)
	fmt.Printf("\n %+v \n", doctorPeriodDuration)
	// Test periods sub treatment duration
	for periodKey := range doctorPeriodDuration {
		fmt.Printf("\n periodKey: %v", periodKey)
		for doctorKey := range doctorPeriodDuration[periodKey] {
			fmt.Printf("\n doctorKey: %v", doctorKey)
			if doctorPeriodDuration[periodKey][doctorKey] >= maxDuration {
				delete(doctorPeriodDuration[periodKey], doctorKey)
			} else {
				totalDuration := doctorPeriodDuration[periodKey][doctorKey] + subTreatmentDuration
				fmt.Printf("\n %v %v | %v + %v = %v", periodKey, doctorKey, doctorPeriodDuration[periodKey][doctorKey], subTreatmentDuration, totalDuration)

				index := 0
				for subPeriod := totalDuration; subPeriod > 0; subPeriod -= maxDuration {
					// Map cannot get by index thereforce make periodList can be get by index
					periodIndex := getIndexByValue(periodList, periodKey)
					nextPeriodIndex := periodIndex + index
					if periodIndex < 0 || nextPeriodIndex > len(periodList)-1 {
						// When last period but subPeriod still more than 0
						if subPeriod > 0 {
							delete(doctorPeriodDuration[periodKey], doctorKey)
						}

						break
					}

					nextPeriod := periodIndex + index
					totalPeriod := 0
					if totalDuration > maxDuration {
						totalPeriod = maxDuration
					} else {
						totalPeriod = subPeriod
					}

					isUseMoreThanOnePeriod := index > 0
					nextPeriodHasTask := doctorPeriodDuration[periodList[nextPeriod]][doctorKey] != 0
					nextPeriodMoreThanTotalPeriod := (maxDuration - doctorPeriodDuration[periodList[nextPeriod]][doctorKey]) < totalPeriod

					if isUseMoreThanOnePeriod && nextPeriodHasTask && nextPeriodMoreThanTotalPeriod {
						delete(doctorPeriodDuration[periodKey], doctorKey)
					}

					totalDuration -= maxDuration
					index += 1
				}
			}
		}
	}

	fmt.Printf("\n %+v \n", doctorPeriodDuration)

	// ช๋วงเวลาไหนไม่มีหมอให้ pop ออก
	for index, resPeriod := range resPeriods {
		if len(doctorPeriodDuration[resPeriod]) < 1 {
			resPeriods = append(resPeriods[:index], resPeriods[index+1:]...)
		}
	}

	fmt.Printf("\n %+v \n", resPeriods)

	// this work
	resPeriods = utils.SortTimes(resPeriods)
	index := 0
	for _, resPeriod := range resPeriods {
		if len(doctorPeriodDuration[resPeriod]) < 1 {
			resPeriods = utils.RemoveItemFromArray(resPeriods, index)
			index -= 1
		}
		index += 1
	}

	fmt.Printf("\n %+v \n", resPeriods)
}

func getIndexByValue[T comparable](slice []T, value T) int {
	for i, v := range slice {
		if v == value {
			return i
		}
	}
	return -1
}
